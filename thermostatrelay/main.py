import machine, ds18x20, onewire
import socket, time, os, ure

prelay = machine.Pin(15, machine.Pin.OUT)
pdallas = ds18x20.DS18X20(onewire.OneWire(machine.Pin(12)))
pled = machine.Pin(2, machine.Pin.OUT)
pled.high()  # off

thermtemp = min([float(f)  for f in os.listdir()  if ure.match("[0-9.]+$", f)], default=18.0)
pdall = sorted(pdallas.scan(), key=lambda X: tuple(X))

def loop():
    n = 0
    prevrelaystate = False
    prelay.value(prevrelaystate)
    while True:
        pdallas.convert_temp()
        time.sleep(0.8)
        tempsall = [ pdallas.read_temp(pd)  for pd in pdall ]
        temprep = sum(tempsall)/len(tempsall)
        print(n, temprep, tempsall)
        n += 1
        relaystate = (temprep < thermtemp)
        if relaystate != prevrelaystate:
            print("Relay to", relaystate, "at temp", temprep, "crossing", thermtemp)
            prelay.value(relaystate)
            prevrelaystate = relaystate
            
        ticks = int(abs(temprep - thermtemp)*4 + 1)
        if ticks >= 5:
            ticks = (ticks - 4)//4 + 4
        for i in range(ticks):
            pled.low()
            time.sleep(0.005)
            pled.high()
            time.sleep(0.1)
        
loop()

"""
static const uint8_t SDA = 4;
static const uint8_t SCL = 5;

static const uint8_t LED_BUILTIN = 2;
static const uint8_t BUILTIN_LED = 2;

static const uint8_t D0   = 16;
static const uint8_t D1   = 5;
static const uint8_t D2   = 4;
static const uint8_t D3   = 0;
static const uint8_t D4   = 2;
static const uint8_t D5   = 14;
static const uint8_t D6   = 12;
static const uint8_t D7   = 13;
static const uint8_t D8   = 15;
static const uint8_t RX   = 3;
static const uint8_t TX   = 1;
"""

