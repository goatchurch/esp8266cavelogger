import socket, time, os, ure, machine, network

pled = machine.Pin(2, machine.Pin.OUT)
pswitch = machine.Pin(14, machine.Pin.IN)

s20endpointipnumber = '192.168.0.162'  # could find this by sf.scan()

myipnumber = ('192.168.4.1', 80)
if "wifi.txt" in os.listdir():
    wnet, wpass = open("wifi.txt").read().split()

    # then pull out the wifi name and password from this file
    sf = network.WLAN(network.STA_IF)
    if not sf.isconnected():
        pled.low()
        sf.active(True)
        sf.connect(wnet, wpass)
        for i in range(100):
            pled.value(i%2)
            time.sleep(0.1)
            if sf.isconnected():
                break
        else:
            print("no connection")
            for i in range(10):
                pled.value(i%2)
                time.sleep(0.5)
    pled.high()
    myipnumber = socket.getaddrinfo("0.0.0.0", 80)[0][-1]
            

def sendupath(upath):
    gpath = bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (upath, s20endpointipnumber), 'utf8')
    s = socket.socket()
    s.connect((s20endpointipnumber, 80))
    s.send(gpath)
    s.send("")   # signifies end of sending
    s.settimeout(10)
    return s.recv(100)


def loop():
    prevval = 0
    while True:
        val = pswitch.value()
        if prevval != val:
            upath = "plug=%d" % val
            prevval = val
            print(upath)
            r = sendupath(upath)
            print(r)

loop()
#sendupath("plug=1")
#sendupath("led=0")

