# README #

Micropython cave data logger on ESP8266 using accelerometer and is very intelligent

### Work in progress ###

(See below for setting up from scratch)

With a sparkfun LSM9DS1 (only I2C broken out, no interrupt generating pins) on the I2C bus
the Acceleromater/Gyro unit is on 0x6B and the Compass is on 0x1E

I have a soldered unit with a light cell on the ADC pin, a dallas temperature sensor 
string on Pin12 and two LEDs on pins 2 and 16.

The http://192.168.4.1/index.html page will get an inteface for checking and resetting the RTC
from your device as well as lining it up against your device.

If you access the ESP with webrepl you can import aw1 and then run aw1.wsall() to serve onto 
the url http://192.168.4.1/a200 for 200 consecutive readings of the accelerometer into 
a csv file.  (Same for (g)yros, (c)ompass, and (t)emperatures.)

The pendule jupyter notebook will call this url and then plot your xyz readings of the device.
It can also conduct FFT and find the peak frequencies.  This can be used to look for pendulum swings 
and other oscillations.

### Stuff to do ###

* Find a waterproof container that can take the adafruit boards and power supplies as they are (without any 
board designs) that are likely to get data for about a day.

* The ultra-low-power change monitoring using a Schmitt trigger and programmed DAC/potentiometer to get an 
immediate interrupt signal when there is a light change, temperature change (relative to an thermistor), or motion.

* Design a circuit board for surface mounted ESP, one of these LSM9DS1 accelerometers (distant enough to 
rely on temp sensor pressed against the wall), power circuits, light cell, LEDs and commission some

* Ensure we've got memory management (for logging), proof of sleep modes that are low power, 
wake up interrupts from the accelerometer and light (so that we can have extended sleep periods) 
and begin coding first basic version for release.

* Build the boards and fit to caves and/or shoes for step measurements, say.


### Setting up from scratch or reflashing ###

Download latest from https://micropython.org/download#esp8266 and downloading esp8266-20161110-v1.8.6.bin

pip2 install esptool

$ esptool.py --port /dev/ttyUSB0 erase_flash

$ esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 binaries/esp8266-20170108-v1.8.7.bin --flash_mode dio
  or
$ esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -fm dio 0 binaries/esp8266-20170611-v1.9.1.bin

Then need to log in to setup the webrepl
$ picocom -b115200 /dev/ttyUSB0 

then do import webrepl_setup
and it asks for a password:  wpass

this makes webrepl_config.py with the password in it

often needs a powercycle here.  

Connect to wifi hotspot password: micropythoN
can ping to 192.168.4.1

>>> import os
>>> os.mkdir("static")
>>> os.mkdir("data")

Disconnect webrepl.  Now you can call 
$ ./uploadall.sh 
on your computer to upload all the files

Let's try:

/home/goatchurch/datalogging/mpy-cross/mpy-cross futils.py


----
We need to make from scratch the micropython so that we can have precompiled modules in it

https://gerfficient.com/2016/10/03/esp8266-getting-started/
cd esp-open-sdk
make

export PATH=/home/goatchurch/datalogging/esp-open-sdk/xtensa-lx106-elf/bin:$PATH

cd micropython
make -C mpy-cross



-----
-----
-----
-----
Use webrepl to access it, but with too much wifi it's slow:

Can copy over the 
../webrepl/webrepl_cli.py main.py 192.168.4.1:

---------
make this: https://github.com/micropython/micropython/tree/master/esp8266

Now make a micropython build
https://github.com/pfalcon/esp-open-sdk

clone the https://github.com/micropython/micropython
export PATH=/home/goatchurch/datalogging/esp-open-sdk/xtensa-lx106-elf/bin:$PATH

go into micropython:
git submodule update --init








