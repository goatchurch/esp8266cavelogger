import machine, neopixel, time, ustruct

# Important: if you use Pin15 it can interfere with the bootup condition if pulled high by the level shifter
# Pin 2 and 0 are also related to bootloading.  
# Pin 16 has to connect to RST if you want it to wake itself out of sleep mode

p = machine.Pin(13)
N = 81
n = neopixel.NeoPixel(p, N)

i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))
i2c.writeto(0x6B, b'\x20\xC0')  

def readaccelerometer():
    i2c.writeto(0x6B, b'\x20\xC0')  
    while True:   # loop to wait for readings to be ready (at 60Hz)
        i2c.writeto(0x6B, b'\x27')
        st = ord(i2c.readfrom(0x6B, 1))  # (IG_XL,IG_G,INACT,BOOT_STATUS,TDA,GDA,XLDA) states whether a reading is ready
        if st & 1:
            break
    i2c.writeto(0x6B, b'\x28')
    s = i2c.readfrom(0x6B, 6)
    return ustruct.unpack("<hhh", s)

prevacc = (0,0,0)
def checkaccsmash():
    global prevacc
    acc = readaccelerometer()
    dacc = sum((a-b)**2  for a, b in zip(acc, prevacc))
    if dacc > 10000**2:
        print(dacc, "acc", acc, "prevacc", prevacc)
        ccol = tuple(max(0, min(255, int((a+16384)/32768*256)))  for a in acc)
        for i in range(0, N, 2):
            n[i] = ccol
        n.write()
    prevacc = acc

def snake(col, Nl, sltime):
    m = time.ticks_ms()
    for i in range(N+Nl):
        if i < N:
            n[i] = (255, 255, 255)
        if 0 <= i-1 < N:
            n[i-1] = col
        if 0 <= i-Nl < N:
            n[i-Nl] = (0,0,0)
        n.write()
        time.sleep(sltime)
        checkaccsmash()
    print("sleeptime", sltime, "looptime", (time.ticks_ms() - m)/(N+Nl)*0.001, "seconds")

col = [80, 1, 233]
Nl = 4
sltime = 0.1


def loop():
    global col, Nl, sltime  # otherwise the variables referenced are local and uninitialized
    while True:
        snake(col, Nl, sltime)
        for i in range(3):
            col[i] = (col[i] + 71*i) % 256
        Nl = (((Nl - 2) + 1) % 5) + 2
        sltime = (sltime + 0.19) % 0.5

loop()   # so you can call it again after control-C




