import sys, time
from machine import UART

# set up the serial line to the baud rate at both ends
u = UART(0, 9600)

def conn():
    u.init(9600)
    time.sleep_ms(50)
    u.write("\n")
    time.sleep_ms(500)
    u.write("\nb750000\n")
    time.sleep_ms(50)
    u.init(750000)
    time.sleep_ms(3500)
    u.write("\n")
    time.sleep_ms(500)
    u.write("\nd20\n")   # set the delay loop
    time.sleep_ms(1500)
    u.write("\na23s0\n") # set the analog slot pin23 slot 0 for the pulse meter
    time.sleep_ms(1500)

def conn1():
    u.init(9600)
    time.sleep_ms(50)
    u.write("\n")
    time.sleep_ms(500)
    u.write("\nb9600\n")
    time.sleep_ms(50)
    u.init(9600)
    time.sleep_ms(3500)
    u.write("\n")
    time.sleep_ms(500)
    u.write("\nd100\n")


def h():
    ts = [ ]
    np = 0
    while 1:
        s = u.read()
        if not s:
            continue
        try:
            s = s.decode("utf8")
            i = s.index("n")
            n = int(s[i+1:i+9], 16)
        except ValueError:
            continue
        ts.append(n-np)
        np = n
        if len(ts) == 20:
            print(ts)
            ts.clear()
            
def g():
    ts = [ ]
    np = 0
    while 1:
        s = u.read()
        if not s:
            continue
        try:
            s = s.decode("utf8")
            i = s.index("a")
            a = int(s[i+1:i+4], 16)
        except ValueError:
            continue
        ts.append(a)
        if len(ts) == 20:
            print(ts)
            ts.clear()



########
# webserver components
import socket, ure
s = None
def initwebserver():
    global s
    s = socket.socket()
    s.bind(('192.168.4.1', 80))
    s.listen(5)  # 5 connections
    s.settimeout(30)   # normal timeout in seconds

def checkforhttprequest(accepttimeout=1.0):
    s.settimeout(accepttimeout)
    try:
        cl, addr = s.accept()  # we want this to timeout shortly so it is cooperative
    except OSError:
        return None
    cl.settimeout(30)  # apply longer timeout on the connection once it's been accepted

    # decode the headers incoming
    fcl = cl.makefile('rwb', 0)
    m = ure.match("GET /(\S*) ", fcl.readline())
    while fcl.readline().strip():
        pass
    rpath = m and m.group(1) or ""
    n = 100
    if rpath[:1] == "n":
        n = int(rpath[1:])

    #print("got", [rpath])
    cl.sendall("HTTP/1.0 200 OK\r\n\r\n")
    cl.sendall("n%d,a\r\n" % n)
    for i in range(n):
        for j in range(10000):
            l = u.read()
            if l:
                break
        try:
            l = l.decode("utf8")
            ai = l.index("a")
            a = int(l[ai+1:ai+4], 16)
        except ValueError:
            continue
        cl.sendall("%d,%d\r\n" % (i, a))
    
    cl.close()
    return None


conn()   # make the serial connection
initwebserver()
while 1:
    checkforhttprequest()
    
