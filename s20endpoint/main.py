# Code that loads onto Sonoff S20 plug and responds to http requests 
# like 192.168.0.196/plug=1  or led=0

# the existence of "wifi.txt" means we log on to the DoESLiverpool network
# you can find it using
# sudo arp-scan 192.168.0.0/24 | grep f5 
#   where the f5 is from the wifi hotspot name

import socket, time, os, ure, machine, network

pplug = machine.Pin(12, machine.Pin.OUT)
pled = machine.Pin(13, machine.Pin.OUT)
pled.high()  # for off (which is value=1)
pbutton = machine.Pin(0, machine.Pin.IN)

pled2 = machine.Pin(2, machine.Pin.OUT)  # this is the huzzah esp device if you are using that instead of the S20
pled2.high()  # for off (which is value=1)


def ConnectToDoESNetwork():
    # then pull out the wifi name and password from this file
    si = network.WLAN(network.STA_IF)
    if si.isconnected():
        print("already connected to something")
        return True
    
    try:
        doeswifiname = max((sc  for sc in si.scan()  if sc[0][:4] == b'DoES'), key=lambda X: X[3])[0]  # X[3]=RSSI (received signal strength)
    except ValueError:
        print("No DoESWifi found")
        return False
        
    print("strongest DoESWifi signal", doeswifiname)
    pled.low()
    si.active(True)
    si.connect(doeswifiname, "decafbad00")
        
    for i in range(100):
        pled.value(i%2)
        pled2.value(i%2)
        time.sleep(0.1)
        if (i%10)==0:
            print("connecting", i)
        if si.isconnected():
            print("\n*** Connected!!!")
            return True
        pled.high()
        pled2.high()
    for i in range(10):
        print("no connection")
        pled.value(i%2)
        pled2.value(i%2)
        time.sleep(0.5)
    pled.low()
    pled2.low()
    return False

myipnumber = ('192.168.4.1', 80)
if "wifi.txt" in os.listdir():
    if ConnectToDoESNetwork():
        myipnumber = socket.getaddrinfo("0.0.0.0", 80)[0][-1]  # this doesn't work anyway
print("myipnumber", myipnumber)
    
s = socket.socket()
s.bind(myipnumber)
s.listen(5)  # 5 connections
cl, addr = None, None



def checkforhttprequest():
    global cl, addr
    s.settimeout(0.2)   # wait loop in seconds when looking for httprequest to come in
    try:
        cl, addr = s.accept()
    except OSError:
        return None     # gets here when wait loop times out
    cl.settimeout(10.0)  # apply longer timeout on the connection once it's been accepted
        
    # receive the http request and look for the GET url path
    fcl = cl.makefile('rwb', 0)
    try:
        m = ure.match("GET /(\S*) ", fcl.readline())
        while fcl.readline().strip():
            pass  # throw away other headers
        rpath = m and m.group(1) or ""
    except OSError as e:
        print(e)
        return None
    print("got", [rpath])

    # now match the command type for plug and led control
    mcomm = ure.match("(plug|led)=([01])", rpath)
    if mcomm:
        pstate = int(mcomm.group(2))
        p = pplug
        ps = pstate
        if mcomm.group(1) == "led":
            ps = 1 - pstate  # light pin inverted
            p = pled
            
        print("current state", p, p.value(), ps)
        if p.value() != ps:
            p.value(ps)   # the actual action that turns on/off plug or led
            if p == pled and pled2 is not None:   # and the second light 
                pled2.value(ps)   
            response = '%s now %d' % (mcomm.group(1), pstate)
        else:
            response = '%s already %d' % (mcomm.group(1), pstate)
    elif rpath == "" or rpath == "static/index.html":
        response = 'commands are /[plug|led]=[0|1]'
    else:
        response = 'unknown command %s' % rpath

    try:
        cl.sendall("HTTP/1.0 200 OK\r\n")
        cl.sendall("Content-Type: text/plain\r\n\r\n")
        cl.sendall("jsonpresponse('%s')" % response)    # makes it jsonp type
        cl.close()
    except OSError as e:
        print(e)
    return None
        

def loop():
    prevbuttvalue = 0
    while True:
        checkforhttprequest()
        if prevbuttvalue != pbutton.value():
            prevbuttvalue = pbutton.value()
            print(pbutton, pbutton.value())
        
loop()
