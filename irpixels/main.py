
# 1 Make code into its own class or module
# 2 precalculate the eeprom data so we don't need the whole panel (but could print out the chipID numbers)
# 2a make a graph of past temperatures
# 3 remove unnecessary arrays like alpha_ij
# 4 startup on plugin should always work
# 5 solder sockets and onyo stripboard; print a handle
# 6 allow access to temperatures on the web
# 7 diode to drop voltage to 2.6

import machine, time, math, ustruct
import ssd1306, mlx90621

d5 = machine.Pin(14, 1)
d2 = machine.Pin(4)
d1 = machine.Pin(5)
i2c = machine.I2C(scl=d1, sda=d2)

o = None

def initoled():
    global o
    d5.on()
    time.sleep_ms(1)
    d5.off()
    time.sleep_ms(10)
    d5.on()
    o = ssd1306.SSD1306_I2C(128, 64, i2c, 0x3d, False)

# spiral fill of a 8x8 square
def cell(x, y, s):
    r = min(255, int(s*64))
    x = x*8
    y = y*8
    if r < 4:
        if r > 0:  o.framebuf.pixel(x+3, y+3, 1)
        if r > 1:  o.framebuf.pixel(x+4, y+3, 1)
        if r > 2:  o.framebuf.pixel(x+4, y+4, 1)
        return
    elif r < 16:
        n = 2
    elif r < 36:
        n = 4
    else:
        n = 6
        
    r -= n**2
    rx, ry = x+4-n//2, y+4-n//2
    if r <= n:
        o.framebuf.fill_rect(rx, ry, n, n, 1)
        o.framebuf.hline(rx, ry-1, r, 1)
    elif r <= 2*n + 1:
        o.framebuf.fill_rect(rx, ry-1, n, n+1, 1)
        o.framebuf.vline(rx+n, ry-1, r - n, 1)
    elif r <= 3*n + 2:
        o.framebuf.fill_rect(rx, ry-1, n+1, n+1, 1)
        w = r-(2*n + 1)
        o.framebuf.hline(rx+n+1-w, ry+n, w, 1)
    else:
        o.framebuf.fill_rect(rx, ry-1, n+1, n+2, 1)
        w = min(r-(3*n + 2), n+2)
        o.framebuf.vline(rx-1, ry+n+1-w, w, 1)
        
    

#spi = machine.SPI(1, baudrate=8000000, polarity=0, phase=0)
#oled = ssd1306.SSD1306_SPI(128, 64, spi, machine.Pin(3), machine.Pin(0), machine.Pin(16))


# https://forum.micropython.org/viewtopic.php?f=16&t=1705&sid=a941eec9bce447f4046c2bd164953481&start=10
#from machine import Pin, SPI
#spi = SPI(miso=Pin(12), mosi=Pin(13, Pin.OUT), sck=Pin(14, Pin.OUT))




xi = 0
def panelplot():
    global xi
    xi += 1
    o.fill(0)
    for x in range(16):
        for y in range(4):
            s = abs(math.sin(math.radians((x+xi)*20)) + math.cos(math.radians((y+xi*0.3)*60)))
            cell(x, y, (s/2.1)**2)
    o.text("%.3f %.3f"%(xi, yi), 12, 40)
    o.show()

m = mlx90621.MLX90621(i2c)

time.sleep(0.5)
initoled()
o.fill(1)
o.show()
m.initialise()
def mmplot():
    m.measure()
    tmin = min(m.temperatures)
    tmax = max(tmin+0.001, max(m.temperatures))
    o.fill(0)
    for i in range(64):
        cell(i//4, (3-(i%4)), ((m.temperatures[i] - tmin)/(tmax - tmin))**2)
    o.text("%.3f %.3f"%(tmin, tmax), 12, 40)
    print(tmin, tmax)
    o.show()
    
while 1:  mmplot()

while 1: panelplot()

#o.fill(0)
#for i in range(64):  cell(i//4, i%4, i/64)
#o.show()
