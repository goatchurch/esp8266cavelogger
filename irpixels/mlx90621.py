import math, ustruct

# looking down tag at top going clockwise
# gnd vdd sda scl

# code based on https://github.com/robinvanemden/MLX90621_Arduino_Processing/tree/master/readTemperatures
# (there are bugs in that one, eg on line 141 
# alpha_cp = (256 * self.eepromData[CAL_alphaCP_H] + self.eepromData[CAL_alphaCP_L]) / (pow(2, CAL_A0_SCALE) * pow(2, (3 - resolution)));

# there are probably other bugs

# 2 precalculate the eeprom data so we don't need the whole panel (but could print out the chipID numbers)

VTH_L               = const(0xDA)
VTH_H               = const(0xDB)
KT1_L               = const(0xDC)
KT1_H               = const(0xDD)
KT2_L               = const(0xDE)
KT2_H               = const(0xDF)
KT_SCALE            = const(0xD2)

SET_DISP            = const(0xae)
CAL_ACOMMON_L       = const(0xD0)
CAL_ACOMMON_H       = const(0xD1)
CAL_ACP_L           = const(0xD3)
CAL_ACP_H           = const(0xD4)
CAL_BCP             = const(0xD5)
CAL_alphaCP_L       = const(0xD6)
CAL_alphaCP_H       = const(0xD7)
CAL_TGC             = const(0xD8)
CAL_AI_SCALE        = const(0xD9)
CAL_BI_SCALE        = const(0xD9)
CAL_A0_L            = const(0xE0)
CAL_A0_H            = const(0xE1)
CAL_A0_SCALE        = const(0xE2)
CAL_DELTA_A_SCALE   = const(0xE3)
CAL_EMIS_L          = const(0xE4)
CAL_EMIS_H          = const(0xE5)


class MLX90621:
    def __init__(self, i2c):
        self.i2c = i2c
        self.resolution = 0
        self.eepromData = None
        self.Tambient = 0
        self.temperatures = [0]*64
        self.irData = bytearray(128)
        self.ptat = 0
        self.cpix = 0
        self.initialise()
        
    def initialise(self):
        self.readEEPROM();
        self.writeTrimmingValue();
        self.setConfiguration();

    def measure(self):
        if self.checkConfig():
            self.readEEPROM();
            self.writeTrimmingValue();
            self.setConfiguration();
        self.readPTAT();
        self.readIR();
        self.calculateTA();
        self.readCPIX();
        self.calculateTO();

    def setConfiguration(self):
        Hz_LSB = 0b00111010;  # 16 frames
        defaultConfig_H = 0b01000110;  
        self.i2c.writeto_mem(0x60, 0x03, bytearray([Hz_LSB - 0x55, Hz_LSB, defaultConfig_H - 0x55, defaultConfig_H]));
        self.resolution = (self.readConfig() & 0x30) >> 4

    def readEEPROM(self):
        self.eepromData = self.i2c.readfrom_mem(0x50, 0x00, 256)

    def writeTrimmingValue(self):
        self.i2c.writeto_mem(0x60, 0x04, bytearray([self.eepromData[0xF7] - 0xAA, self.eepromData[0xF7], 0x56, 0x00]));


    def calculateTA(self):
        k_t1_scale = (self.eepromData[KT_SCALE] & 0xF0) >> 4;
        k_t2_scale = (self.eepromData[KT_SCALE] & 0x0F) + 10;
        
        v_th = 256 * self.eepromData[VTH_H] + self.eepromData[VTH_L];
        if (v_th >= 32768.0):
            v_th -= 65536.0;
        v_th = v_th / pow(2, (3 - self.resolution));
        k_t1 = 256 * self.eepromData[KT1_H] + self.eepromData[KT1_L];
        if (k_t1 >= 32768.0):
            k_t1 -= 65536.0;
            
        k_t1 /= (pow(2, k_t1_scale) * pow(2, (3 - self.resolution)));
        k_t2 = 256 * self.eepromData[KT2_H] + self.eepromData[KT2_L];
        if (k_t2 >= 32768.0):
            k_t2 -= 65536.0;
            
        k_t2 /= (pow(2, k_t2_scale) * pow(2, (3 - self.resolution)));
        self.Tambient = ((-k_t1 + math.sqrt((k_t1)**2 - (4 * k_t2 * (v_th - self.ptat)))) / (2 * k_t2)) + 25.0;


    def calculateTO(self):
        emissivity = (256 * self.eepromData[CAL_EMIS_H] + self.eepromData[CAL_EMIS_L]) / 32768.0;
        a_common = 256 * self.eepromData[CAL_ACOMMON_H] + self.eepromData[CAL_ACOMMON_L];
        if (a_common >= 32768):
            a_common -= 65536;
        alpha_cp = (256 * self.eepromData[CAL_alphaCP_H] + self.eepromData[CAL_alphaCP_L]) / (pow(2, self.eepromData[CAL_A0_SCALE]) * pow(2, (3 - self.resolution)));
        
        a_i_scale = (self.eepromData[CAL_AI_SCALE] & 0xF0) >> 4;
        b_i_scale = self.eepromData[CAL_BI_SCALE] & 0x0F;
        a_cp = 256 * self.eepromData[CAL_ACP_H] + self.eepromData[CAL_ACP_L];
        if (a_cp >= 32768.0):
            a_cp -= 65536.0;
        a_cp /= pow(2, (3 - self.resolution));
        b_cp = self.eepromData[CAL_BCP];
        if (b_cp > 127.0):
            b_cp -= 256.0;
        b_cp /= (pow(2, b_i_scale) * pow(2, (3 - self.resolution)));
        tgc = self.eepromData[CAL_TGC];
        if (tgc > 127.0):
            tgc -= 256.0;
        tgc /= 32.0;
        v_cp_off_comp = self.cpix - (a_cp + b_cp * (self.Tambient - 25.0));
        for i in range(64):
            a_ij = (a_common + self.eepromData[i] * pow(2, a_i_scale)) / pow(2, (3 - self.resolution));
            b_ij = self.eepromData[0x40 + i];
            if (b_ij > 127):
                b_ij -= 256;
            b_ij = b_ij / (pow(2, b_i_scale) * pow(2, (3 - self.resolution)));
            ird = ustruct.unpack("<h", self.irData[i*2:i*2+2])[0]
            if ird == 0:
                self.temperatures[i] = 0
                continue
            v_ir_off_comp = ird - (a_ij + b_ij * (self.Tambient - 25.0));
            v_ir_tgc_comp = v_ir_off_comp - tgc * v_cp_off_comp;
            alpha_ij = ((256 * self.eepromData[CAL_A0_H] + self.eepromData[CAL_A0_L]) / pow(2, self.eepromData[CAL_A0_SCALE]));                              
            alpha_ij += (self.eepromData[0x80 + i] / pow(2, self.eepromData[CAL_DELTA_A_SCALE]));                          
            alpha_ij = alpha_ij / pow(2, 3 - self.resolution);                                 
            v_ir_norm = v_ir_tgc_comp / (alpha_ij - tgc * alpha_cp);
            v_ir_comp = v_ir_norm / emissivity;
            ll = (v_ir_comp + pow((self.Tambient + 273.15), 4))
            if ll > 0:
                self.temperatures[i] = math.exp((math.log(ll)/4.0)) - 273.15;

    def readIR(self):
        self.i2c.writeto(0x60, b"\x02\x00\x01\x80", False)
        self.i2c.readfrom_into(0x60, self.irData)

    def readPTAT(self):
        self.i2c.writeto(0x60, b"\x02\x40\x00\x01", False)
        self.ptat = ustruct.unpack("<H", self.i2c.readfrom(0x60, 2))[0]

    def readCPIX(self):
        self.i2c.writeto(0x60, b"\x02\x41\x00\x01", False)
        self.cpix = ustruct.unpack("<h", self.i2c.readfrom(0x60, 2))[0]

    def readConfig(self):
        self.i2c.writeto(0x60, b"\x02\x92\x00\x01", False)
        config = ustruct.unpack("<H", self.i2c.readfrom(0x60, 2))[0]
        return config;

    def checkConfig(self):
        return not ((self.readConfig() & 0x0400) >> 10)

