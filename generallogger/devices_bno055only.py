import time, ustruct, ure, math, sys
import machine

# Version which only has the BNO055 functions so we don't run out of memory

# Module containing the interfaces to all devices identifiable by scanning on startup with 
# the initdevices() function (to be called after things like pindallas has been set)

# Functions are as minimal as possible with i2c devices identified by their address codes
# RTC interface is via isodates and jsepoch (javascript time units) for compatibility with webpages

# GPIO0 and GPIO2 set what mode we boot up in.  If you use them they both need to be pulled up high
# GPIO15 needs a pull down (or it won't reboot properly).  Avoid using these pins anyway if you can.  
# (you can change the clock frequency with machine.freq(160000000) from 80000000)

# Wemos pins: GPIO12=D6, GPIO13=D7, GPIO5=D1, GPIO4=D2, GPIO15=D8

i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))   # NodeMCU: (D1=SCL, D2=SDA)
readlightpin = machine.Pin(13, machine.Pin.OUT)  # wired to the light resistor, read from adc pin

p2 = machine.Pin(2, machine.Pin.OUT)
p2.on()   # blue light off
epoch1970 = 946684800000   # esp8266 has 2000 epoch

readlightpause = 0   # pause delay in seconds after readlightpin set high; see also devices.enableloglightvalues
adc = machine.ADC(0)


rtc = machine.RTC()  # this is the internal crap RTC
def rtcdatetime():
    if sys.platform == "LoPy":
        return rtc.now()
    else:
        return rtc.datetime()

# dallas wiring: flat face towards you, left is ground, right is live, middle is signal with a 4.7k pullup resistor onto the live
pindallas = None   # machine.Pin(12)  [would be good for pin0 as that one should be pulled up anyway]
dallasobj = None
dallasscanned = None

scannedi2c = None
bmp180consts = None
RTCnotset = rtcdatetime()[0] < 2017  # we are in right year (maybe we've experienced a soft-reset)
nlightson = set()

# these are off the string in Julian's house listed in order
#dallasqorder = { 3891110100093507112:8, 864691150498112552:1, 7926335366213288488:3, 17365880185180280616:5, 
#                 3891110100094395176:6, 2522015813366565160:4, 4179340476246253096:2, 5908722733156329256:7 }
dallasqorder = None

def initdevices():
    global scannedi2c, bmp180consts, RTCnotset, nlightson, dallasobj, dallasscanned
    
    scannedi2c = i2c.scan()
    res = [ ]

    if 0x28 in scannedi2c:
        k = i2c.readfrom_mem(0x28, 0x00, 6)
        res.append("BNO055 sensor SW_REV_ID: %s.%s" %(hex(k[4]), hex(k[5])))
        
        i2c.writeto_mem(0x28, 0x3D, b'\x00')     # config mode
        i2c.writeto_mem(0x28, 0x3E, b'\x00')     # PWR_MODE, normal
        i2c.writeto_mem(0x28, 0x3B, b'\x00')     # UNIT_SEL, celsius, UDegrees and m/s^2
        i2c.writeto_mem(0x28, 0x3D, b'\x0c')     # back to NDOF mode

    for a in scannedi2c:
        if a not in [0x68, 0x77, 0x32, 0x69, 0x6B, 0x1E, 0x48, 0x40, 0x70, 0x28]:
            res.append("%s: Unknown I2C device" % hex(a))
            
    return res


def LED4digitnumber(n):
    pass

#
# Realtime clock code, handles isodates, javascriptepoch timestamps (in milliseconds), 
# and interface to the DS3231 as well as the internal RTC
# 
def rtctojsepoch(bforceinternalRTC=False):
    def rhex(h):  return (h>>4)*10 + (h&0x0f)
    if 0x68 in scannedi2c and not bforceinternalRTC:
        r = i2c.readfrom_mem(0x68, 0x00, 7)
        year, month, day = rhex(r[6])+2000, rhex(r[5]), rhex(r[4])
        hour, minute, second = rhex(r[2]), rhex(r[1]), rhex(r[0])
        microsecond = 0
    elif sys.platform == "LoPy":
        year, month, day, hour, minute, second, microsecond, D1 = rtc.now()
    else:
        year, month, day, D1, hour, minute, second, microsecond = rtc.datetime()
    micropythonepoch = time.mktime((year, month, day, hour, minute, second, -1, -1))
    return micropythonepoch*1000 + microsecond + epoch1970   # esp8266 has 2000 epoch

def jsepochtortc(jsepoch):
    global RTCnotset
    assert sys.platform != "LoPy"
    def dhex(v):  return chr(((v//10)<<4) + (v%10))
    micropythonepoch = (jsepoch - epoch1970)//1000
    microsecond = jsepoch % 1000
    year, month, day, hour, minute, second, d1, d2 = time.localtime(micropythonepoch)
    if 0x68 in scannedi2c:
        i2c.writeto(0x68, chr(0) + dhex(second) + dhex(minute) + dhex(hour))
        i2c.writeto(0x68, chr(4) + dhex(day) + dhex(month) + dhex(year-2000))
    rtc.datetime((year, month, day, -1, hour, minute, second, microsecond))
    RTCnotset = False
    
def isodatetojsepoch(isodate):
    mtime = ure.match("(\d\d\d\d)-(\d\d)-(\d\d)[T ](\d\d)[:c](\d\d)[:c](\d\d)[\.d](\d\d\d)", isodate)
    year, month, day, hour, minute, second, microsecond = int(mtime.group(1)), int(mtime.group(2)), int(mtime.group(3)), int(mtime.group(4)), int(mtime.group(5)), int(mtime.group(6)), int(mtime.group(7)) 
    micropythonepoch = time.mktime((year, month, day, hour, minute, second, -1, -1))
    return micropythonepoch*1000 + microsecond + epoch1970

def jsepochtoisodate(jsepoch):
    micropythonepoch = (jsepoch - epoch1970)//1000
    microsecond = jsepoch % 1000
    year, month, day, hour, minute, second, d1, d2 = time.localtime(micropythonepoch)
    return "{0:04d}-{1:02d}-{2:02d}T{3:02d}:{4:02d}:{5:02d}.{6:03d}".format(year, month, day, hour, minute, second, microsecond)
    

def readlight():
    return 0

def lightset(lightnumber, lightintensity):   # led lights
    if 0x32 in scannedi2c:
        i2c.writeto(0x32, chr(0x16+lightnumber) + chr(lightintensity))
    else:
        if lightintensity == 0:
            if lightnumber in nlightson:
                nlightson.remove(lightnumber)
        else:
            nlightson.add(lightnumber)
        if (len(nlightson) % 2) == 1:
            p2.off()
        else:
            p2.on()
        
        
# butterworth filtering code (useful to apply to random noise sensors, like CO2sensor)
class ABfilter:
    def __init__(self, b, a):
        self.b = b
        self.a = a
        assert len(self.b) == len(self.a)
        self.xybuff = None
        self.xybuffpos = 0

    def addfiltvalue(self, x):
        n = len(self.b)
        if self.xybuff is None:
            self.xybuff = [x]*(n*2)
        self.xybuff[self.xybuffpos] = x 
        j = self.xybuffpos 
        y = 0 
        for i in range(n):
            y += self.xybuff[j]*self.b[i] 
            if i != 0:
                y -= self.xybuff[j+n]*self.a[i] 
            j = j-1 if j!=0 else n-1
        if self.a[0] != 1:
            y /= self.a[0]
        self.xybuff[self.xybuffpos+n] = y 
        self.xybuffpos = self.xybuffpos+1 if self.xybuffpos!=n-1 else 0
        Dj = (n-1 if (self.xybuffpos == 0) else self.xybuffpos-1)
        assert self.xybuff[Dj+n] == y
        return y 

    def getvalue(self):
        j = n-1 if (xybuffpos == 0) else xybuffpos-1
        return xybuff[j+n]; 
    
def BNO055calibstat():
    calibstat = i2c.readfrom_mem(0x28, 0x35, 1)[0]
    #print("sys:", (calibstat>>6)&0x03, "gyr:", (calibstat>>4)&0x03, "acc:", (calibstat>>2)&0x03, "mag:", calibstat&0x03)
    return calibstat

def BNO055quat():  # returns qw, qx, qy, qz
    return ustruct.unpack("<hhhh", i2c.readfrom_mem(0x28, 0x20, 8))
    #accx, accy, accz = ustruct.unpack("<hhh", i2c.readfrom_mem(0x28, 0x28, 6))
    #gravx, gravy, gravz = ustruct.unpack("<hhh", i2c.readfrom_mem(0x28, 0x2E, 6))
    
def BNO055pitchrollorient():  
    q0, q1, q2, q3 = ustruct.unpack("<hhhh", i2c.readfrom_mem(0x28, 0x20, 8))
    riqsq = q0*q0 + q1*q1 + q2*q2 + q3*q3 
    iqsq = 1/riqsq 
    
    r02 = q0*q2*2 * iqsq
    r13 = q1*q3*2 * iqsq
    sinpitch = r13 - r02

    r01 = q0*q1*2 * iqsq
    r23 = q2*q3*2 * iqsq 
    sinroll = r23 + r01 
     
    r00 = q0*q0*2 * iqsq
    r11 = q1*q1*2 * iqsq
    r03 = q0*q3*2 * iqsq
    r12 = q1*q2*2 * iqsq
    a00 = r00 - 1 + r11   
    a01 = r12 + r03  
    rads = math.atan2(a00, -a01) 
    northorient = 180 - math.degrees(rads) 
    return math.degrees(math.asin(sinpitch)), math.degrees(math.asin(sinroll)), northorient

# over-rides the function above, which 
def BNO055pitchrollorient():  
    heading, roll, pitch = ustruct.unpack("<hhh", i2c.readfrom_mem(0x28, 0x1A, 6))
    return pitch/16, roll/16, heading/16
    
