import machine, socket, ure, ustruct, time, os
import timefuncs, devices


#devices.pindallas = machine.Pin(12)   # this is wemos pin D6
devices.CO2smoother = "disable"

pled = machine.Pin(2, machine.Pin.OUT)
pled.on()  # on-board blue led off
devices.readlightpause = 0  # light with voltage divider, or transistor controlled water pressure detector that may take a moment for the current to settle

for i in range(2):   # give it a couple of tries
    try:
        devmsgs = devices.initdevices()
        break
    except OSError as e:
        print("initdevices failed", i)
        time.sleep(0.4)
print("\n".join(devmsgs))
del devmsgs

import datalogger

# pin 13 (wemos pin D7) 
datalogger.enableloglightvalues = True  # used for light levels and water levels (pin A0)

datalogger.initdatalogger()

import webserver
webserver.initwebserver()

# make directories and move static files into directory
stuff = os.listdir()
for d in ['static', 'data']:
    if not d in stuff:
        os.mkdir(d)
for f in stuff:
    if ure.search('\.html$|\.js$|\.css$|\.jpg$', f):
        os.rename(f, 'static/'+f)


Doserr = None
nloop = 0
def wsall():
    global Doserr, nloop
    print("listening ")
    while True:
        try:
            webserver.checkforhttprequest(accepttimeout=1.0)
        except OSError as e:
            print(e, "checkforhttprequest")
            Doserr = e
            devices.lightset(1, 0)
        except OverflowError as e:
            print(e, "checkforhttprequest")   # can be found if a bad number gets into devices.jsepochtoisodate(devices.rtctojsepoch())
            Doserr = e
            devices.lightset(1, 0)
            
        try:
            datalogger.MakeSensorReadings(not timefuncs.RTCnotset)   # only autostore when RTC is set
        except OSError as e:
            print("makesensorreadingserror", e)
        
        nloop += 1
        if (nloop % 10) == 0 or nloop < 10 or timefuncs.RTCnotset:
            devices.lightset(8, 9)
            time.sleep(0.01)
            devices.lightset(8, 0)
            if timefuncs.RTCnotset:  # extra flashing when we don't have the time set yet
                time.sleep(0.2)
                devices.lightset(8, 9)
                time.sleep(0.01)
                devices.lightset(8, 0)
                
    
wsall()  


