import socket, time, os, ure
import ubinascii, network
import gc
import timefuncs
import devices
import datalogger


def decoderequestheaders(cl):
    fcl = cl.makefile('rwb', 0)
    m = ure.match("GET /(\S*) ", fcl.readline())
    while fcl.readline().strip():
        pass
    return m and m.group(1) or ""

def servestaticfile(cl, lpath):
    try:
        fin = open(lpath, "r")
        fsize = os.stat(lpath)[6]
    except OSError:
        cl.sendall("HTTP/1.0 404 Not found\r\n\r\n")
        cl.sendall("file '%s' not found" % lpath)
        return None
        
    cl.sendall("HTTP/1.0 200 OK\r\n")
    lfext = ure.search("\.(\w+)$", lpath).group(1)
    if lfext == "js":
        cl.sendall("Content-Type: application/javascript\r\n")
        cl.sendall("Cache-Control: max-age=31536000\r\n")
    elif lfext == "css":
        cl.sendall("Content-Type: text/css\r\n")
        cl.sendall("Cache-Control: max-age=31536000\r\n")
    elif lfext == "jpg":
        cl.sendall("Content-Type: image/jpeg\r\n")
        cl.sendall("Cache-Control: max-age=31536000\r\n")
    elif lfext == "png":
        cl.sendall("Content-Type: image/png\r\n")
        cl.sendall("Cache-Control: max-age=31536000\r\n")
    elif lfext == "dcc":
        cl.sendall("Content-Type: application/binary\r\n")
    elif lfext == "html":
        cl.sendall("Content-Type: text/html\r\n")
    else:
        cl.sendall("Content-Type: text/plain\r\n")
    cl.sendall("Content-Length: %d\r\n" % fsize)
    cl.sendall("\r\n")
    
    print("bytes", fsize)
    nsum = 0
    n = 0
    while True:
        bs = fin.read(10)  # was 256 but it threw an allocation error for 440bytes
        if not bs:
            break
        nsum += len(bs)
        if (n % 50) == 0:
            print("seg", n, len(bs), nsum/fsize)
        cl.sendall(bs)
        n += 1
    return None

def servetime(cl, lpath):
    mtime = ure.match("(set)?time(?:[/=](\d\d\d\d-\d\d-\d\d[T ]\d\d:\d\d:\d\d\.\d\d\d))?", lpath)
    if mtime.group(2):
        if timefuncs.RTCnotset or mtime.group(1):
            e = timefuncs.isodatetojsepoch(mtime.group(2))
            timefuncs.jsepochtortc(e)
            print("to set time", timefuncs.rtcdatetime())
    cl.sendall("HTTP/1.0 200 OK\r\n")
    cl.sendall("Content-Type: text/plain\r\n\r\n")
    e = timefuncs.rtctojsepoch()
    cl.sendall(timefuncs.jsepochtoisodate(e))


def servebasedata(cl):
    cl.sendall("HTTP/1.0 200 OK\r\n")
    cl.sendall("Content-Type: text/plain\r\n\r\n")
    cl.sendall("batmemory alloc=%d free=%d\n" % (gc.mem_alloc(), gc.mem_free()))
    cl.sendall("battime %s\n" % timefuncs.jsepochtoisodate(timefuncs.rtctojsepoch()).replace("T", " "))
    if 0x48 in devices.scannedi2c:
        cl.sendall("battemp %f\n" % devices.readtmp102())
    cl.sendall("batlight %d\n" % devices.readlight())
    if 0x6B in devices.scannedi2c:
        cl.sendall("batacc x=%d y=%d z=%d\n" % devices.readvectorsensor("a"))
        cl.sendall("batgyro x=%d y=%d z=%d\n" % devices.readvectorsensor("g"))
    if 0x1E in devices.scannedi2c:
        cl.sendall("batmag x=%d y=%d z=%d\n" % devices.readvectorsensor("c"))
    if 0x77 in devices.scannedi2c:
        t10, p = devices.readbmp180()
        cl.sendall("batbmppressure %d\n" % p)
        cl.sendall("batbmptemp %.1f\n" % (t10/10))
    if 0x40 in devices.scannedi2c:
        cl.sendall("bathumid %.2f\nbathtemp %.2f\n" % devices.SI7021humiditytemp())
    if 0x69 in devices.scannedi2c:
        try:  cl.sendall("batCO2 %d\n" % devices.readCO2())
        except OSError: pass
    if 0x28 in devices.scannedi2c:
        try:  cl.sendall("batBNO055 p=%.0f r=%.0f n=%.0f\n" % devices.BNO055pitchrollorient())
        except MemoryError: pass
    if devices.dallasscanned and devices.dallasobj:
        cl.sendall("batdallas %s\n" % " ".join("%.3f" % devices.dallasobj.read_temp(pd)  for pd in devices.dallasscanned))
        devices.dallasobj.convert_temp()   # we do the conversions in an immediate loop cycle
    cl.sendall("batmacaddress %s" % ubinascii.hexlify(network.WLAN().config("mac")[-3:]).decode("utf8"))

        
def sendimmediatedata(cl, vs, n):
    cl.sendall("HTTP/1.0 200 OK\r\n\r\n")
    
    Fftime = timefuncs.rtctojsepoch()
    t0 = time.ticks_ms()
    
    if vs == "battemp":
        cl.sendall("time%d,t\n" % Fftime)
        for i in range(n):
            cl.sendall("%d,%.2f\r\n" % (time.ticks_ms()-t0, devices.readtmp102()))
            time.sleep(0.1)
            
    elif vs == "batlight":
        cl.sendall("time%d,light\n" % Fftime)
        for i in range(n):
            cl.sendall("%d,%d\r\n" % (time.ticks_ms()-t0, devices.readlight()))
            time.sleep(0.1)
            
    elif vs in ["batacc", "batgyro", "batmag"]:
        cl.sendall("time%d,x,y,z\r\n" % Fftime)
        for i in range(n):
            sv = devices.readvectorsensor(vs[3])
            cl.sendall("%d,%d,%d,%d\r\n" % (time.ticks_ms()-t0, sv[0], sv[1], sv[2]))

    elif vs == "batbmppressure":
        cl.sendall("time%d,pressure\n" % Fftime)
        for i in range(n):
            cl.sendall("%d,%d\r\n" % (time.ticks_ms()-t0, devices.readbmp180()[1]))
            
    elif vs == "batbmptemp":
        cl.sendall("time%d,temp\n" % Fftime)
        for i in range(n):
            cl.sendall("%d,%.2f\r\n" % (time.ticks_ms()-t0, devices.readbmp180()[0]*0.1))
            
    elif vs == "bathumid":
        cl.sendall("time%d,temp\n" % Fftime)
        for i in range(n):
            cl.sendall("%d,%.2f\r\n" % (time.ticks_ms()-t0, devices.SI7021humiditytemp()[0]))

    elif vs == "bathtemp":
        cl.sendall("time%d,temp\n" % Fftime)
        for i in range(n):
            cl.sendall("%d,%.2f\r\n" % (time.ticks_ms()-t0, devices.SI7021humiditytemp()[1]))

    elif vs == "batCO2":
        cl.sendall("time%d,temp\n" % Fftime)
        for i in range(n//10):
            time.sleep(2.0)   # documented measurement interval
            cl.sendall("%d,%d\r\n" % (time.ticks_ms()-t0, devices.readCO2()))

    elif vs == "batdallas":
        cl.sendall("time%d,temp\n" % Fftime)
        for i in range(n//10):
            devices.dallasobj.convert_temp()
            time.sleep(0.8)
            sds = ",".join("%.3f" % devices.dallasobj.read_temp(pd)  for pd in devices.dallasscanned)
            cl.sendall("%d,%s\r\n" % (time.ticks_ms()-t0, sds))

    elif vs == "batBNO055":
        cl.sendall("time%d,c,p,r,n\n" % Fftime)
        for i in range(n):
            time.sleep(0.05)   # 20x a second (can be up to 100Hz
            devices.lightset(1, 9)
            cl.sendall("%d,%d," % ((time.ticks_ms()-t0), devices.BNO055calibstat()))
            cl.sendall("%.2f,%.2f,%.2f\r\n" % devices.BNO055pitchrollorient())
            devices.lightset(1, 0)

    else:
        cl.sendall("unknown %s\n" % vs)


def servefilelist(cl): 
    cl.sendall("HTTP/1.0 200 OK\r\n")
    cl.sendall("Content-Type: text/html\r\n\r\n")
    cl.sendall("<table>\n")
    cl.sendall("<tr><th>Name</th><th>Size</th></tr>\n")
    for fname in os.listdir():
        fsize = os.stat(fname)[6]
        if fsize == 0:
            try:
                for fname1 in os.listdir(fname):
                    ffname = fname+"/"+fname1
                    ffsize = os.stat(ffname)[6]
                    cl.sendall('<tr><td><a href="/{0:s}">{0:s}</a></td><td>{1:d}</td></tr>\n'.format(ffname, ffsize))
                continue
            except:
                pass
        cl.sendall('<tr><td><a href="/{0:s}">{0:s}</a></td><td>{1:d}</td></tr>\n'.format(fname, fsize))
    cl.sendall("</table>\n")
        

########
# webserver components
s = None

def initwebserver():
    global s
    s = socket.socket()
    s.bind(('192.168.4.1', 80))
    s.listen(5)  # 5 connections
    s.settimeout(30)   # normal timeout in seconds

def checkforhttprequest(accepttimeout):
    s.settimeout(accepttimeout)
    try:
        cl, addr = s.accept()  # we want this to timeout shortly so it is cooperative
    except OSError:
        return None
    cl.settimeout(30)  # apply longer timeout on the connection once it's been accepted
        
    # read the headers
    rpath = decoderequestheaders(cl)
    print("got", [rpath])
    
    if not ure.match("flashlight", rpath):
        devices.lightset(1, 9)
    
    if rpath == "":
        print("rerecting to static/index")
        cl.sendall("HTTP/1.1 301 Moved\r\n")
        cl.sendall("Location: /static/index.html\r\n")
    
    elif ure.match("csvdata/", rpath):
        m = ure.match("csvdata/([^/]*)(?:/([ldcphfb]*))?$", rpath)  # \d doesn't work in [ ]
        lfname = m.group(1)
        contents = m.group(2) or "d"
        datalogger.StreamSensorReadings(cl, lfname, contents)

    elif ure.match("list$", rpath):
        if datalogger:
            datalogger.tempcloseopenfile()  # just in case
        servefilelist(cl)

    elif ure.match("deletedata/", rpath):  # must be before the file match
        m = ure.match("deletedata/(.*)$", rpath)
        datalogger.DeleteData(cl, m.group(1))

    elif ure.match(".*?\.(html|js|ico|css|csv|py|bi.|map|png|jpg|dcc)$", rpath):
        servestaticfile(cl, rpath)
        
    elif ure.match("(set)?time", rpath):
        servetime(cl, rpath)

    elif ure.match("datastatus", rpath):
        datalogger.DataStatus(cl)

    elif ure.match("basedata$", rpath):
        servebasedata(cl)

    elif ure.match("flashlight", rpath):
        m = ure.match("flashlight/g(\d+)/(\d+)ms/i(\d+)", rpath)
        if m:
            devices.lightset(int(m.group(1)), int(m.group(3)))
            time.sleep(int(m.group(2))/1000)
            devices.lightset(int(m.group(1)), 0)
        cl.sendall("HTTP/1.0 200 OK\r\n")
        cl.sendall("Content-Type: text/plain\r\n\r\n")
        cl.sendall("ok")
        
    elif ure.match("immediate/", rpath):
        m = ure.match("immediate/(bat.*?)/n=(\d+)", rpath)
        sendimmediatedata(cl, m.group(1), int(m.group(2)))

    else:
        print("unrecognized get request", [rpath])
        cl.sendall("HTTP/1.0 404 Not found\r\n\r\n")
        cl.sendall("path '%s' not recognized" % rpath)

    devices.lightset(1, 0)
    cl.close()
    return None

    
        
