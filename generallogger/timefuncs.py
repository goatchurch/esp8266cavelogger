import time, ustruct, ure, math, sys
import machine
import sys

if sys.platform == "LoPy":
    epoch1970 = 0
else:
    epoch1970 = 946684800000   # esp8266 has 2000 epoch

rtc = machine.RTC()  # this is the internal crap RTC
def rtcdatetime():
    if sys.platform == "LoPy":
        return rtc.now()
    else:
        return rtc.datetime()

RTCnotset = rtcdatetime()[0] < 2017  # we are in right year (maybe we've experienced a soft-reset)
i2c0x68 = None  # set to i2c if 0x68 DS3231 clock exists

#
# Realtime clock code, handles isodates, javascriptepoch timestamps (in milliseconds), 
# and interface to the DS3231 as well as the internal RTC
# 
def rtctojsepoch(bforceinternalRTC=False):
    def rhex(h):  return (h>>4)*10 + (h&0x0f)
    if i2c0x68 and not bforceinternalRTC:
        r = i2c0x68.readfrom_mem(0x68, 0x00, 7)
        year, month, day = rhex(r[6])+2000, rhex(r[5]), rhex(r[4])
        hour, minute, second = rhex(r[2]), rhex(r[1]), rhex(r[0])
        microsecond = 0
    elif sys.platform == "LoPy":
        year, month, day, hour, minute, second, microsecond, D1 = rtc.now()
    else:
        year, month, day, D1, hour, minute, second, microsecond = rtc.datetime()
    micropythonepoch = time.mktime((year, month, day, hour, minute, second, -1, -1))
    return micropythonepoch*1000 + microsecond + epoch1970   # esp8266 has 2000 epoch

def jsepochtortc(jsepoch):
    global RTCnotset
    assert sys.platform != "LoPy"
    def dhex(v):  return chr(((v//10)<<4) + (v%10))
    micropythonepoch = (jsepoch - epoch1970)//1000
    microsecond = jsepoch % 1000
    year, month, day, hour, minute, second, d1, d2 = time.localtime(micropythonepoch)
    if i2c0x68:
        i2c0x68.writeto(0x68, chr(0) + dhex(second) + dhex(minute) + dhex(hour))
        i2c0x68.writeto(0x68, chr(4) + dhex(day) + dhex(month) + dhex(year-2000))
    rtc.datetime((year, month, day, -1, hour, minute, second, microsecond))
    RTCnotset = False
    
def isodatetojsepoch(isodate):
    mtime = ure.match("(\d\d\d\d)-(\d\d)-(\d\d)[T ](\d\d)[:c](\d\d)[:c](\d\d)[\.d](\d\d\d)", isodate)
    year, month, day, hour, minute, second, microsecond = int(mtime.group(1)), int(mtime.group(2)), int(mtime.group(3)), int(mtime.group(4)), int(mtime.group(5)), int(mtime.group(6)), int(mtime.group(7)) 
    micropythonepoch = time.mktime((year, month, day, hour, minute, second, -1, -1))
    return micropythonepoch*1000 + microsecond + epoch1970

def jsepochtoisodate(jsepoch):
    micropythonepoch = (jsepoch - epoch1970)//1000
    microsecond = jsepoch % 1000
    year, month, day, hour, minute, second, d1, d2 = time.localtime(micropythonepoch)
    return "{0:04d}-{1:02d}-{2:02d}T{3:02d}:{4:02d}:{5:02d}.{6:03d}".format(year, month, day, hour, minute, second, microsecond)
    
