import ustruct, time, os, sys
import timefuncs, devices

MAXDATFILESIZE = 50000
enableloglightvalues = False

# b  barometer 0x77 happens to be both bmp180 and ms5611 devices
# f  figaro CO2sensor
# d  dallas onewire temperature sensor(s)
# h  Si7021 humidity sensor
# l  Light analog in
def Fupackcode(letter, codenumber):
    if letter == 'b' and codenumber == 0x77:  
        return '<hH'
    if letter == 'h' and codenumber == 0x40:
        return '<HH'
    if letter == 'f' and codenumber == 0x69:
        return '<H'
    if letter == 'l':
        return '<H'
    if letter == 'd':
        return '<'+'h'*codenumber
    return None

def Fstorereading(letter, codenumber):
    if letter == 'b' and codenumber == 0x77:
        return (lambda r: (r[0], r[1] - 50000))
    if letter == 'h' and codenumber == 0x40:
        return (lambda r: (int(r[0]*100), int(r[1]*100)))
    if letter == 'f' and codenumber == 0x69:
        return (lambda r: (r,))
    if letter == 'l':
        return (lambda r: (r,))
    if letter == 'd':
        return (lambda r: [int(d*16)  for d in r])
        
def Fcsvreading(letter, codenumber):  
    #if letter == 'b' and codenumber == 0x77:
    #    return (lambda r: "%.1f,%d" % (r[0]*0.1, r[1] + 50000))
    if letter == 'b' and codenumber == 0x77:
        return (lambda r: "%d" % (r[1] + 50000))
    if letter == 'h' and codenumber == 0x40:
        return (lambda r: "%.2f,%.2f" % (r[0]*0.01, r[1]*0.01))
    if letter == 'f' and codenumber == 0x69:
        return (lambda r: "%d" % r[0])
    if letter == 'l':
        return (lambda r: "%d" % r[0])
    if letter == 'd':
        return (lambda r: ",".join("%.2f" % (d/16)  for d in r))


# class for managing reading and writing buffers of a particular sensor type
# header of each buffer is s(letter) s(codenumber) H(bufferlength) L(jsepoch//1000)
class SensorBytePageLog:
    def __init__(self):
        self.bbuffer = None
        
    def setuppacking(self, letter, codenumber):
        self.letter = letter   # [b]arometer, [d]allas
        self.codenumber = codenumber   # i2caddress or n for dallas
        self.upackcode = Fupackcode(self.letter, self.codenumber)
        self.lenupackcode = ustruct.calcsize(self.upackcode)
        self.Lstorereading = Fstorereading(self.letter, self.codenumber)
        self.Lcsvreading = Fcsvreading(self.letter, self.codenumber)
        
    def Fcsvheading(self):
        #if self.letter == 'b' and self.codenumber == 0x77:
        #    return 'temp,baro'
        if self.letter == 'b' and self.codenumber == 0x77:
            return 'baro'
        if self.letter == 'h' and self.codenumber == 0x40:
            return 'humid,temp'
        if self.letter == 'f' and self.codenumber == 0x69:
            return 'CO2ppm'
        if self.letter == 'l':
            return 'AnalogIn'
        if self.letter == 'd':
            return ",".join("temp%d"%i  for i in range(self.codenumber))
        return None
        
    def initbuffer(self, jsepoch):
        if self.bbuffer is None:
            self.bbuffer = bytearray(120)
        self.bbuffpos = 0
        self.jsepoch = jsepoch
        if self.letter == 'b' and self.codenumber == 0x77:
            self.jsepochoffset = 3
        if self.letter == 'h' and self.codenumber == 0x40:
            self.jsepochoffset = 3000
        if self.letter == 'f' and self.codenumber == 0x69:
            self.jsepochoffset = 2000
        if self.letter == 'l':
            self.jsepochoffset = 5000
        if self.letter == 'd':
            devices.dallasobj.convert_temp()   # we do the conversions in an immediate loop cycle
            self.jsepochoffset = 4000
            
    def makedevicereading(self):
        if self.letter == 'b' and self.codenumber == 0x77:
            return devices.readbmp180()
        if self.letter == 'h' and self.codenumber == 0x40:
            return devices.SI7021humiditytemp()
        if self.letter == 'f' and self.codenumber == 0x69:
            res = devices.readCO2smooth() 
            if 0x70 in devices.scannedi2c:
                devices.LED4digitnumber(res)   # we want the CO2 readings on the screen please
            return res if res != 0 else None
        if self.letter == 'l':
            return devices.readlight()
        if self.letter == 'd':
            self.dallasreadings = [ devices.dallasobj.read_temp(pd)  for pd in devices.dallasscanned ]
            devices.dallasobj.convert_temp()   # we do the conversions in an immediate loop cycle
            return self.dallasreadings
            
        return None
        
    def storereading(self, ljsepoch, lreadings):
        i = self.bbuffpos
        i2 = i + 2
        i3 = i2 + self.lenupackcode
        if i3 > len(self.bbuffer):
            return False
        self.bbuffer[i:i2] = ustruct.pack("<H", (ljsepoch - self.jsepoch)//1000)
        self.bbuffer[i2:i3] = ustruct.pack(self.upackcode, *self.Lstorereading(lreadings))
        self.bbuffpos = i3

    def writebuffer(self, fout):
        fout.write(ustruct.pack("<ssHL", self.letter, chr(self.codenumber), self.bbuffpos, self.jsepoch//1000))
        fout.write(memoryview(self.bbuffer)[:self.bbuffpos])
        
    def readbuffer(self, fin):
        head8 = fin.read(8)
        if len(head8) != 8:
            return False
        letter, codenumber, self.bbuffpos, jsepochby1000 = ustruct.unpack("<ssHL", head8)
        self.setuppacking(letter.decode(), ord(codenumber))
        self.jsepoch = jsepochby1000*1000
        self.bbuffer = fin.read(self.bbuffpos)

        self.nreadings = len(self.bbuffer)/(2 + self.lenupackcode)
        self.jsepochlastoffset = int(ustruct.unpack("<H", self.bbuffer[-(2+self.lenupackcode):-self.lenupackcode])[0])*1000
        
        return True
        
    def sendcsvlines(self, cl, Fftime, bbuffer):
        i = 0
        while i < self.bbuffpos:
            i2 = i + 2
            ljsepoch = ustruct.unpack("<h",  bbuffer[i:i2])[0]*1000 + self.jsepoch
            i3 = i2 + self.lenupackcode
            if ljsepoch > Fftime:  # quick hack to remove negative values inserted before the clock got set
                cl.sendall("%d" % (ljsepoch - Fftime))
                cl.sendall(",")
                cl.sendall(self.Lcsvreading(ustruct.unpack(self.upackcode, bbuffer[i2:i3])))
                cl.sendall("\n")
            i = i3

def SensorBytePageLogSpecific(letter, codenumber, jsepoch):
    pagelog = SensorBytePageLog()
    pagelog.setuppacking(letter, codenumber)
    pagelog.nextreadingepoch = 0
    pagelog.initbuffer(jsepoch)
    return pagelog


#####
# File handling pointers (could be turned into a set of module functions and not a class)
datadirectory = "data"
fout = None
fname = None
Fftime = 0
foutsize = 0
nprintcount = 0
jsepoch = 0
pagelogs = [ ]
dallaspagelog = None

def initdatalogger():
    global fout, fname, Fftime, foutsize, nprintcount, jsepoch, dallaspagelog
    fout = None
    fname = None
    Fftime = 0
    foutsize = 0
    nprintcount = 0
    jsepoch = timefuncs.rtctojsepoch()
    pagelogs.clear()
    
    if 0x77 in devices.scannedi2c:   # bmp180 barometer
        pagelogs.append(SensorBytePageLogSpecific('b', 0x77, jsepoch))
    if 0x40 in devices.scannedi2c:   # si7021 humidity
        pagelogs.append(SensorBytePageLogSpecific('h', 0x40, jsepoch))
    if devices.dallasscanned:        # dallas temperature
        pagelogs.append(SensorBytePageLogSpecific('d', len(devices.dallasscanned), jsepoch))
        dallaspagelog = pagelogs[-1]
    if 0x69 in devices.scannedi2c:   # figaro CO2 meter
        pagelogs.append(SensorBytePageLogSpecific('f', 0x69, jsepoch))
    if enableloglightvalues:         # light cell, or current response water pressure gauge
        pagelogs.append(SensorBytePageLogSpecific('l', 0x00, jsepoch))
    
    
def MakeSensorReadings(autostore=True):
    global fout, fname, Fftime, foutsize, nprintcount, jsepoch
    jsepoch = timefuncs.rtctojsepoch()
    for pagelog in pagelogs:
        if jsepoch >= pagelog.nextreadingepoch:
            try:
                lreadings = pagelog.makedevicereading()
                if lreadings is not None:
                    pagelog.storereading(jsepoch, lreadings)
            except OSError as e:
                print("ReadingError", pagelog.letter, hex(pagelog.codenumber), e)
            except Exception as e:
                print("other error", e)
                pass
            pagelog.nextreadingepoch = jsepoch + pagelog.jsepochoffset
            print("pagelogbuff", pagelog.letter, pagelog.bbuffpos)
        
        if autostore and pagelog.bbuffpos >= 90:
            print("pagelog-storing", pagelog.letter)
            EnsureFileOpen()
            foutsize += 8 + pagelog.bbuffpos
            pagelog.writebuffer(fout)
            pagelog.initbuffer(jsepoch)

    if fout is not None and foutsize >= MAXDATFILESIZE:
        fout.close()
        fout = None
        fname = None
    nprintcount += 1
    if (nprintcount % 20) == 0:
        print("readings", fname, foutsize)
        

def StreamSensorReadings(cl, lfname, contents):
    tempcloseopenfile()
    cl.sendall("HTTP/1.0 200 OK\r\n")
    cl.sendall("Content-Type: text/plain\r\n")
    cl.sendall("\r\n")

    ff = datadirectory + "/" + lfname
    fin = open(ff, "rb")

    mletter = contents[0]

    sbpl = SensorBytePageLog()
    Fftime = timefuncs.isodatetojsepoch(lfname)
    bheadersent = False
    while sbpl.readbuffer(fin):
        print("recblock", sbpl.letter, hex(sbpl.codenumber), sbpl.bbuffpos)
        if sbpl.letter == mletter:
            if not bheadersent:
                cl.sendall("time%d,%s\n" % (Fftime, sbpl.Fcsvheading()))
                bheadersent = True
            sbpl.sendcsvlines(cl, Fftime, sbpl.bbuffer)
    if not bheadersent:
        cl.sendall("Nothing!")
    fin.close()
        
def EnsureFileOpen():
    global fout, fname, Fftime, foutsize, nprintcount, jsepoch
    if fout and foutsize >= MAXDATFILESIZE:
        fout.close()
        fout = None
        fname = None
    if not fout:
        if fname:
            try:
                foutsize = os.stat(fname)[6]
                if foutsize < MAXDATFILESIZE:
                    print("re-opening", fname)
                    fout = open(fname, "ab")
            except OSError:
                fout = None
        if not fout:
            e = timefuncs.rtctojsepoch()
            eiso = timefuncs.jsepochtoisodate(e)
            fiso = eiso.replace(":", "c").replace(".", "d")+".dcc"  # this is where the data files are made
            print(fiso)
            Fftime = timefuncs.isodatetojsepoch(fiso)
            fname = datadirectory + "/" + fiso
            print("working with", fname)
            fout = open(fname, "wb")
            foutsize = 0
        
def tempcloseopenfile():
    global fout
    if fout:
        fout.close()
        fout = None
        
def DataStatus(cl):
    tempcloseopenfile()
    cl.sendall("HTTP/1.0 200 OK\r\n")
    cl.sendall("Content-Type: text/plain\r\n")
    cl.sendall("\r\n")
    tsz = 0
    fdats = os.listdir(datadirectory)
    fdats.sort(reverse=True)
    cl.sendall("datafile,size\n")
    for ff in fdats:
        sz = os.stat(datadirectory+"/"+ff)[6]
        cl.sendall("%s,%d\n" % (ff, sz))

def DeleteData(cl, lfname):
    tempcloseopenfile()
    ff = datadirectory + "/" + lfname
    os.remove(ff)
    cl.sendall("HTTP/1.0 200 OK\r\n")
    cl.sendall("Content-Type: text/plain\r\n")
    cl.sendall("\r\n")
    cl.sendall("ok")
        
    
def ScanDatafile(ff):
    tempcloseopenfile()
    fff = datadirectory+"/"+ff
    fin = open(fff, "rb")
    
    sbpl = SensorBytePageLog()
    blocksinfo = { }  #  { (letter,codenumber) : [ jsepochstart, jsepochend, nreadings ] }
    while sbpl.readbuffer(fin):
        bikey = (sbpl.letter, sbpl.codenumber)
        if bikey not in blocksinfo:
            blocksinfo[bikey] = [ sbpl.jsepoch, sbpl.jsepoch, 0 ]
        blocksinfo[bikey][1] = sbpl.jsepoch + sbpl.jsepochlastoffset
        blocksinfo[bikey][2] += int(sbpl.nreadings)
    fin.close()
    return blocksinfo
    
def ScanAllDatafiles(self):
    tempcloseopenfile()
    fdats = os.listdir(datadirectory)
    fdats.sort()
    for ff in fdats:
        print()
        print(ff)
        blocksinfo = ScanDatafile(ff)
        for (letter, codenumber), v in blocksinfo.items():
            print("  %s,%s: n=%d  %s %dsecs" % (letter, hex(codenumber), v[2], timefuncs.jsepochtoisodate(v[0]), (v[1]-v[0])//1000))
            
