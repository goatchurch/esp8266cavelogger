import time, ustruct, ure, math, sys
import timefuncs
import machine

i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))   # NodeMCU: (D1=SCL, D2=SDA)
readlightpin = machine.Pin(13, machine.Pin.OUT)  # wired to the light resistor, read from adc pin

p2 = machine.Pin(2, machine.Pin.OUT)
p2.on()   # blue light off

readlightpause = 0   # pause delay in seconds after readlightpin set high; see also devices.enableloglightvalues
adc = machine.ADC(0)

scannedi2c = None
bmp180consts = None
nlightson = set()

dallasscanned = None
C1, C2, C3, C4, C5, C6 = 0,0,0,0,0,0


def initdevices():
    global scannedi2c, C1, C2, C3, C4, C5, C6
    
    scannedi2c = i2c.scan()
    res = [ ]

    if 0x77 in scannedi2c:
        C1 =  ustruct.unpack('>H', i2c.readfrom_mem(0x77, 0xA2, 2))[0]
        C2 =  ustruct.unpack('>H', i2c.readfrom_mem(0x77, 0xA4, 2))[0]
        C3 =  ustruct.unpack('>H', i2c.readfrom_mem(0x77, 0xA6, 2))[0]
        C4 =  ustruct.unpack('>H', i2c.readfrom_mem(0x77, 0xA8, 2))[0]
        C5 =  ustruct.unpack('>H', i2c.readfrom_mem(0x77, 0xAA, 2))[0]
        C6 =  ustruct.unpack('>H', i2c.readfrom_mem(0x77, 0xAC, 2))[0]

        res.append("0x77 is MS5611 barometer")

    for a in scannedi2c:
        if a not in [0x68, 0x77, 0x32, 0x69, 0x6B, 0x1E, 0x48, 0x40, 0x70, 0x28]:
            res.append("%s: Unknown I2C device" % hex(a))
            
    return res

def readMS5611():
    i2c.writeto(0x77, b'\x48')
    time.sleep_ms(9)
    D1 = i2c.readfrom_mem(0x77, 0x00, 3)
    D1 = D1[0]*0x10000 + D1[1]*0x100 + D1[2]
    i2c.writeto(0x77, b'\x58')
    time.sleep_ms(9)
    D2 = i2c.readfrom_mem(0x77, 0x00, 3)
    D2 = D2[0]*0x10000 + D2[1]*0x100 + D2[2]
    
    dT = D2 - C5 * 0x100
    TEMP = 2000 + dT*C6/0x00800000 
    OFF = C2*0x10000 + dT*C4/0x80 
    SENS = C1*0x8000 + dT*C3/0x100
    
    if (TEMP < 2000):
        T2 = dT*dT/0x80000000 
        ra = TEMP - 2000 
        ra = ra*ra 
        OFF -= 5*ra/2 
        SENS -= 5*ra / 4 
        if (TEMP < -1500):
            rb = TEMP - (-1500); 
            rb = rb*rb; 
            OFF -= 7*rb; 
            SENS -= 11*rb / 2; 
        TEMP -= T2; 
    
    Pr = (SENS * D1 / 0x200000 - OFF) / 0x8000; 
    return TEMP, Pr
    
def readbmp180():  # make compatible
    t, pr = readMS5611()
    return int((t+5)/10), int(pr)

def fexpgen(e):
    x = yield None
    f = x
    while 1:
        f += (x-f)*e
        x = yield f
        
    # old code
    while 1:
        bv = readMS5611()[1]
        y1 = F.send(bv)
        if abs(y-y1)>0.5:
            y=y1
            print(time.ticks_ms(), y)


def readlight():
    return 0

def lightset(lightnumber, lightintensity):   # led lights
    if 0x32 in scannedi2c:
        i2c.writeto(0x32, chr(0x16+lightnumber) + chr(lightintensity))
    else:
        if lightintensity == 0:
            if lightnumber in nlightson:
                nlightson.remove(lightnumber)
        else:
            nlightson.add(lightnumber)
        if (len(nlightson) % 2) == 1:
            p2.off()
        else:
            p2.on()
        
