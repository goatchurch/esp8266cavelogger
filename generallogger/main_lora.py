
# LoPy lorawan logger
# copy code across using this:
# chrome://fireftp/content/fireftp.xul#
# don't forget to connect by wifi first as it doesn't work through the serial
# even though you can access the command line with picocom -b115200 /dev/ttyUSB0

import devices
import lorastuff
import pycom
import time

pycom.heartbeat(False)

# wait until the module has joined the network
while not lorastuff.lora.has_joined():
    print('Not joined yet...')
    pycom.rgbled(0x222200)
    time.sleep(0.5)
    pycom.rgbled(0x020200)
    time.sleep(2.0)

while True:
    try:
        k = devices.initdevices()
        print(k)
        if 0x69 in devices.scannedi2c:  # CO2 meter exists
            break
    except OSError:
        pass
        
    print('Error inittin devices...')
    pycom.rgbled(0x220000)
    time.sleep(0.5)
    pycom.rgbled(0x020000)
    time.sleep(2.0)


while True:
    if 0x48 in devices.scannedi2c:
        r = devices.i2c.readfrom_mem(0x48, 0x00, 2)
        print("tmp102", r[0], r[1]/256)
        lorastuff.uplora(bytes([0x48, r[0], r[1]]))
        
    if 0x69 in devices.scannedi2c:
        try:
            r = devices.i2c.readfrom_mem(0x69, 0x03, 2)
        except OSError as e:
            print(e, "on figaroCO2")
            r = None
        if r:
            v = r[0] + r[1]*256
            if 100 < v < 10000:
                print("figaroCO2", v)
                lorastuff.uplora(bytes([0x69, r[0], r[1]]))
            else:
                print("bad figaroCO2", v)

    pycom.rgbled(0x002000 if r is not None else 0x200000)
    time.sleep(0.1)
    if r is not None:
        pycom.rgbled(0x000200 + int(min(max(0, v-400), 3000)/1000*16))
    else:
        pycom.rgbled(0x020000)
    time.sleep(4.9)
    
# to run the command line subscriber, do:
# mosquitto_sub -h eu.thethings.network -d -t '+/devices/+/up'  -u 'lakelogger3' -P 'ttn-account-v2.25ve38qwoHfx2e2sAyV4J0IfSo2Qc1dl0QRQMadsmgM' -v


#{"app_id":"lakelogger3","dev_id":"lakelogger4","hardware_serial":"70B3D5499D1AB554","port":2,"counter":175,"payload_raw":"adsE","payload_fields":{"CO2":1243},"metadata":{"time":"2017-05-09T17:02:41.551653515Z","frequency":867.1,"modulation":"LORA","data_rate":"SF7BW125","coding_rate":"4/5","gateways":[{"gtw_id":"eui-b827ebfffe7fc86b","timestamp":2550238011,"time":"2017-05-09T17:02:41.343934Z","channel":3,"rssi":-73,"snr":8.2,"latitude":53.402943,"longitude":-2.984601,"altitude":10},{"gtw_id":"eui-008000000000b1f7","timestamp":280169371,"time":"2017-05-09T17:02:37.686781Z","channel":3,"rssi":-91,"snr":10,"latitude":53.40368,"longitude":-2.9824047,"altitude":10}]}}"
#09/05/2017, 18:02:46node: 69aa09b5.27f5d8lakelogger3/devices/lakelogger4/up : msg.payload : string[670] 
