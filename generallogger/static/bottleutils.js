
//
// handle http requests back to the ESP8266 and responsetext sent to callbacks with the
// makerequest(url, callback) function
//
var Dresponsetext = ""; 
var vrequest = new XMLHttpRequest(); 
var vreqcallback = null; 

function gvreqcallback(e) 
{
    if ((vrequest.readyState == 4) && (vrequest.status == 200)) {
        document.getElementById("reqpara").className = "reqprevious"; 
        var lvreqcallback = vreqcallback; 
        vreqcallback = null; 
        Dresponsetext = vrequest.responseText; 
        if (lvreqcallback !== null) 
            lvreqcallback(vrequest.responseText);
    }
}
 
vrequest.addEventListener("readystatechange", gvreqcallback); 
function makerequest(flink, lvreqcallback) 
{
    if (vreqcallback !== null) {
        console.log("not making request", flink, "as still waiting for previous request"); 
        return false; 
    }
    vreqcallback = lvreqcallback; 
    document.getElementById("reqpara").className = "reqcurrent"; 
    document.getElementById("currentrequest").textContent = flink; 
    document.getElementById("currentrequest").setAttribute("href", flink); 
    vrequest.open("GET", flink, true); 
    vrequest.send(); 
    var reqdatetime = (new Date()).toISOString().split("T"); 
    document.getElementById("reqdate").textContent = reqdatetime[0]; 
    document.getElementById("reqtime").textContent = reqdatetime[1].substring(0, 9)
    return true; 
}


//
// Delayed loading of the 100kb dygraph graphing library
//
var bdyngraphlibraryrequested = false; 
function loaddgraphlibrary() 
{
    var elhead = document.getElementsByTagName("head"); 
    var sdyngraph = document.createElement("script"); 
    sdyngraph.src = "dygraph.min.js"; 
    sdyngraph.onload = function() { 
        document.getElementById("currentrequest").textContent = "((Dyngraph library loaded))"; 
    }
    elhead[0].appendChild(sdyngraph); 
}

//
// fetchbasedata and continuousbasedata buttons actioning and loading their responses into the webpage
// this splits the set of lines and values, sets them in the panel, and unhides those which exist
//
function returnedbasedata(responsetext) 
{
    var rlines = responsetext.split("\n"); 

    // set all the elements invisible
    var batstats = document.getElementById("batstats"); 
    var libatstats = batstats.getElementsByTagName("li"); 
    for (var i = 0; i < libatstats.length; i++)  
        if (libatstats[i].id != 'tnowlength')
            libatstats[i].style.display = 'none'; 

    // unhide and fill value on any elements we have
    for (var i = 0; i < rlines.length; i++) {
        var si = rlines[i].search(" "); 
        if (si == -1)  continue; 
        var batvname = rlines[i].substring(0, si); 
        var batvalue = rlines[i].substring(si+1); 
        var eli = document.getElementById(batvname);    
        eli.style.display = ''; 
        var el = eli.getElementsByTagName("span")[0]; 
        el.innerHTML = batvalue.replace(" ", "<br/>").replace(" ", "<br/>"); 
        
        if (batvname == "batmacaddress")  
            SetBatMacAddress(batvalue);  
        if (batvname == "battime")
            eli.style.background = (batvalue < "2017" ? "pink" : ""); // for warning about not set date
            
    }
    
    if (document.getElementById("datsel").value == "--none--")  // auto-run fetchbasedata
        setTimeout(function() { document.getElementById("datastatus").click() }, 500); 

    if (document.getElementById("continuousbasedata").className == "selected")
        setTimeout(function() { makerequest("/basedata", returnedbasedata) }, 500); 
}

function fetchbasedata(bcontinuousbuttonpressed) 
{
    var elcon = document.getElementById("continuousbasedata"); 
    if (elcon.className == "selected") {
        if (bcontinuousbuttonpressed)
            elcon.className = ""; 
        return; 
    } 
    
    if (bcontinuousbuttonpressed) 
        elcon.className = "selected"; 
    
    makerequest("/basedata", returnedbasedata); 
}


// 
// datafile handling functions 
//
function fetchdatastatus() 
{
    makerequest("/datastatus", function(responsetext) { 
        var rlines = responsetext.split("\n"); 
        var datseloptions = [ ]; 
        for (var i = 1; i < rlines.length; i++) {
            var fields = rlines[i].split(","); 
            if (fields.length == 2) {
                var shn = fields[0].replace("T", " ").replace(/c/g, ":").replace(/d.*/g, ""); 
                datseloptions.push('<option value="', fields[0], '">', shn, " ", fields[1], "bytes", "</option>"); 
            }
        }
        document.getElementById("datsel").innerHTML = datseloptions.join(""); 
        
        if (!bdyngraphlibraryrequested) {
            document.getElementById("currentrequest").textContent = "((Dyngraph library requested))"; 
            document.getElementById("currentrequest").setAttribute("href", ""); 
            bdyngraphlibraryrequested = true; 
            loaddgraphlibrary(); 
        }
    }); 
}


//
// graphing panel code 
// 
var vtime = null; 
var vdata = null; 
var ddata = null; 
var vdatastatus = null; 

/*
var responsetext = "t,n\n1485115226000,8\n1485115228000,9\n1485115229000,7\n";         
//drawgraph(responsetext, "a"); 

// this can also force to use dates by spoofing a data object, but numbers are converted to Date and back again
var responsedata = {
    getColumnRange:     function() { return "what"; }, 
    getNumberOfColumns: function() { return 2; }, 
    getNumberOfRows:    function() { return 300; }, 
    getColumnType:      function(i) { return (i == 0 ? "datetime" : "number"); }, 
    getColumnLabel:     function(i) { return ("sss"+i); }, 
    getValue:           function(j, i) { return (i == 0 ? new Date(1485115226000 + j*1000) : (j%5)); }
}; 
*/

// the actual decision of whether it's a date is made when it checks if the first cell of first row has non-numeric characters in it!!! which is why the F-trick in first cell worked
var toffset = 1485115226000; 
function reltimeparser(x)
{
    var res; 
    if (x[0] == 'F') {
        toffset = parseInt(x.substring(1)); 
        res = toffset; 
    } else {
        res = parseInt(x) + toffset; 
    }
    return res; 
}

var dgraph1 = null; 
var lightsections = [ ]; 
function lightunderlay(canvas, area, g) 
{ 
    canvas.fillStyle = "rgba(255, 255, 102, 1.0)";
    for (var i = 1; i < lightsections.length; i += 2) {
        var xlo = lightsections[i-1]; 
        var xhi = lightsections[i]; 
        var cxlo = g.toDomXCoord(xlo);
        var cxhi = g.toDomXCoord(xhi);
        var cwidth = cxhi - cxlo;
        canvas.fillRect(cxlo, area.y, cwidth, area.h);
    }
}

// going to try to be able to read this as a fake data
function drawgraph(csvdata, vdatareq) 
{
    toffset = parseInt(csvdata.substring(4, csvdata.search(",")))
    if (toffset + 0 != toffset) // fix any NaN cases
        toffset = 1485115226000; 
    
    var yformatter = (vdatareq == 'd' ? 
                        function(y) { return (y/1).toFixed(2); } :  // was /16
                        function(y) { return (y/1).toFixed(0); }); 
    dgraph1 = new Dygraph(document.getElementById("graph1"), csvdata, 
    {
        legend: 'always',
        animatedZooms: true,
        //title: 'Fridge temperatures', 
        //rollPeriod: 5,
        //showRoller: true,
        xValueParser: function(x) { return parseInt(x) + toffset; },
        underlayCallback: lightunderlay, 
        axes: { 
            x: { 
                // can't find these references in V2.  they are in utils which appears not to be exposed
                // However, first element has an F in it, so is not numerical and it interprets as a date
                //valueFormatter: Dygraph.dateValueFormatter,
                //axisLabelFormatter: Dygraph.dateAxisLabelFormatter,
                //ticker: Dygraph.dateTicker
                axisLabelFontSize: 30
            }, 
            y: { 
                valueFormatter: yformatter, // function(x, opts, seriesName) { return (x/16).toFixed(2); }, 
                axisLabelFormatter: yformatter, // function(x, granularity) { return (x/16).toFixed(2); } 
                axisLabelFontSize: 30
            },  
        } 
    });
    
    // force the time value to be rendered as dates (even though it's given all as numbers)
    dgraph1.setXAxisOptions_(true); 
    dgraph1.cascadeDataDidUpdateEvent_();
    dgraph1.predraw_(); 
}
