import machine, socket, time, math, ustruct


## setting up the accelerometer
i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))   # NodeMCU: (D1=SCL, D2=SDA)
scannedi2c = i2c.scan()
if 0x6B in scannedi2c:
	print("0x6B is Gyros/Accelerometer")
	
	# turn on gyros reg(0x10)0x20=(ODR_G,FS_G,0,BWG)=001 00 0 00 gives 65ms ~ 14.9Hz
	# turn on gyros reg(0x10)0x40=(ODR_G,FS_G,0,BWG)=010 00 0 00 gives 59.9Hz at 245deg/sec
	# turn on gyros reg(0x10)0x40=(ODR_G,FS_G,0,BWG)=010 11 0 00 gives 59.9Hz at 2000deg/sec
	i2c.writeto(0x6B, b'\x10\x40')  # these gyros don't seem to work anyway.  give constant values that respond to orienting (0x13), scaling(0x10), enabling(0x1e), but not any any measuring of the environment

	# turn on accelerometer reg(0x20)=(ODR_XL,FS_XL,BW_SCAL_ODR,BW_XL)=110 00 0 00 should give 952Hz, but is overridden by gyros ODR 
	# there are various further settings of FIFO and High and Low pass filters
	i2c.writeto(0x6B, b'\x20\xC0')  

if 0x1E in scannedi2c:
	print("0x1E is Magnetometer(compass)")
	i2c.writeto(0x1E, b'\x22\x00')  

	# turn on magnetometer reg(0x20)=(TEMP_COMP,OM,DO,0,ST)=1 11 110 0 0 (Ultra High Performance, 40Hz)
	#i2c.writeto(0x1E, b'\x20\x58')  
	i2c.writeto(0x1E, b'\x20\x58')  
        
# reading the accelerometer/gyro/compass
Lcsd = { "a":(0x6B, 1, b"\x28"), "g":(0x6B, 2, b"\x18"), "c":(0x1E, 8, b"\x28") }
def readvectorsensor(vs):
    cs, stm, ds = Lcsd[vs] 
    if cs not in scannedi2c:
        return (0,0,0)
    while True:   # loop to wait for readings to be ready (at 60Hz)
        i2c.writeto(cs, b'\x27')
        st = ord(i2c.readfrom(cs, 1))  # (IG_XL,IG_G,INACT,BOOT_STATUS,TDA,GDA,XLDA) states whether a reading is ready
        if st & stm:
            break
    i2c.writeto(cs, ds)
    s = i2c.readfrom(cs, 6)
    sv = ustruct.unpack("<hhh", s)
    return sv


pbeep = machine.PWM(machine.Pin(15))

pled = machine.Pin(2, machine.Pin.OUT)
pled.on()  # off

# generated by: b, a = scipy.signal.butter(3, 0.05, 'low')
b = [ 0.00041655,  0.00124964,  0.00124964,  0.00041655]
a = [ 1.        , -2.6861574 ,  2.41965511, -0.73016535]

thighangledegrees = 60
downtimems = 5000
downtimecountcounts = 30000  # time between squats for the extra count to count

class buttfilter:
    def __init__(self):
        self.n = len(a)
        self.xybuff = [0]*(2*self.n)
        self.xybuffpos = 0

    def addvalueF(self, x):
        self.xybuff[self.xybuffpos] = x; 
        j = self.xybuffpos 
        y = 0; 
        for i in range(self.n):
            y += self.xybuff[j]*b[i] 
            if i != 0:
                y -= self.xybuff[j+self.n]*a[i] 
            if j == 0:
                j = self.n; 
            j -= 1; 

        if a[0] != 1:  y /= a[0]; 
        self.xybuff[self.xybuffpos+self.n] = y; 
        self.xybuffpos += 1;
        if self.xybuffpos == self.n:
            self.xybuffpos = 0; 

        j = (self.n-1  if self.xybuffpos == 0  else self.xybuffpos-1)
        return self.xybuff[j+self.n] 


bx, by, bz = buttfilter(), buttfilter(), buttfilter()
tl0 = time.ticks_ms()
prevdx = 0
tdsquat = 0
tdsquatc = 0
ncount = 0
downtimecount = 0
bsquatdetected = False
tdlastsquat = 0
def loop():
    global tl0, prevdx, tdsquat, tdsquatc, ncount, downtimecount, downtimecount, bsquatdetected, tdlastsquat
    try:
        sv = readvectorsensor("a")
    except OSError:
        return
    ssv = (bx.addvalueF(sv[0]), by.addvalueF(sv[1]), bz.addvalueF(sv[2]))
    dx, dy = math.degrees(-math.atan2(ssv[2], ssv[0])), -math.degrees(math.atan2(ssv[2], ssv[1]))
    tl0p = tl0
    ncount += 1
    tl0 = time.ticks_ms()
    pled.value(0  if (ncount % 200) == 0  else 1)
    
    if abs(dx-prevdx) > 2:
        print("%.0f" % dx)
        prevdx = dx
    if dx >= thighangledegrees:
        pbeep.freq(300 + int(min(600, (dx - thighangledegrees)/(90 - thighangledegrees)*600)))
        if (ncount % 20) == 0:
            pbeep.duty(500)
        elif tdsquat != 0 and tl0-tdsquat > downtimems and 5 < (ncount % 20) < 15:
            pbeep.duty(3)
            bsquatdetected = True
        else:
            pbeep.duty(0)

        if tdsquat != 0:
            for i in range(tdsquatc, (tl0-tdsquat)//1000):
                tdsquatc += 1
                print(".%d." % tdsquatc, end="")   # count the seconds
        else:
            tdsquat = tl0
            tdsquatc = 0
    else:
        if bsquatdetected:
            if tdsquat - tdlastsquat > downtimecountcounts:
                downtimecount = 0
                print("settng squat count to 0")
            tdlastsquat = time.ticks_ms()
            downtimecount += 1
            print("squat count", downtimecount)
            bsquatdetected = False
            time.sleep_ms(100)
            pbeep.freq(1500)
            for i in range(downtimecount//10):
                pbeep.duty(400)
                time.sleep_ms(80)
                pbeep.duty(0)
                time.sleep_ms(90)
            for i in range(downtimecount%10):
                pbeep.duty(400)
                time.sleep_ms(15)
                pbeep.duty(0)
                time.sleep_ms(50)

        tdsquat = 0
        pbeep.duty(0)

while True:
    loop()
    

